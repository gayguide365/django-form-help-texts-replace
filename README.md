django-form-help-texts-replace
==============================

Replace model/form field help text to explicit hepls

Authors
-------
*  Ondrej Sika, <http://ondrejsika.com>, dev@ondrejsika.com

Source
------
* Documentation: <http://ondrejsika.com/docs/django-form-help-texts-replace>
* Python Package Index: <http://pypi.python.org/pypi/django-form-help-texts-replace>
* GitHub: <https://github.com/sikaondrej/django-form-help-texts-replace>
