#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name = "django-form-help-texts-replace",
    version = "1.0.1",
    url = 'http://ondrejsika.com/docs/django-form-help-texts-replace',
    download_url = 'https://github.com/sikaondrej/django-form-help-texts-replace',
    license = 'GNU LGPL v.3',
    description = "",
    author = 'Ondrej Sika',
    author_email = 'ondrej@ondrejsika.com',
    py_modules = ["form_help_texts_replace"],
    install_requires=["django-flexible-form"],
    #packages = find_packages(),
)
